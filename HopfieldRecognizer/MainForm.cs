﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using HopfieldNeuralNetwork;
using System.IO;

namespace HopfieldRecognizer
{
    public partial class frmMain : Form
    {
        private NeuralNetwork NN;
		private HammingNeuralNetwork HNN;
        private int imageDim = Convert.ToInt32(HopfieldRecognizer.Properties.Resources.ImageDimension);
        private bool patternSelected = false;
		private bool patternSelectedHamm = false;

		public frmMain()
        {
            InitializeComponent();
        }

        private void butStworzSiecHopf_Click(object sender, EventArgs e)
        {
            NN = new NeuralNetwork(imageDim*imageDim);
            panelWzorceHopf.Controls.Clear();
            NN.EnergyChanged += new EnergyChangedHandler(NN_EnergyChanged);
            Random rnd = new Random();
            int r = 0;
            imStanSieci.pixels = new int[imageDim, imageDim];
            for (int i = 0; i < imageDim; i++)
                for (int j = 0; j < imageDim; j++)
                {
                    r = rnd.Next(2);
                    if (r == 0) imStanSieci.pixels[i, j] = Color.Black.ToArgb();
                    else if (r == 1) imStanSieci.pixels[i, j] = Color.White.ToArgb();                    
                }
            patternSelected = false;
            butDodajWzor.Enabled = true;
            butRozpoznajHopf.Enabled = false;
            imStanSieci.Visible = true;
            imStanSieci.Invalidate();
        }

        private void NN_EnergyChanged(object sender, EnergyEventArgs e)
        {
            int i = (int)e.NeuronIndex / imageDim;
            int j = e.NeuronIndex % imageDim;
            if (imStanSieci.pixels[i, j] == Color.White.ToArgb()) imStanSieci.pixels[i, j] = Color.Black.ToArgb();
            else if (imStanSieci.pixels[i, j] == Color.Black.ToArgb()) imStanSieci.pixels[i, j] = Color.White.ToArgb();
            imStanSieci.Invalidate();
            Application.DoEvents();
            System.Threading.Thread.Sleep(100);
        }

        private void butDodajWzor_Click(object sender, EventArgs e)
        {
            Image imgPattern;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                imgPattern = Image.FromFile(ofd.FileName);
                if (imgPattern.Width != imageDim || imgPattern.Height != imageDim)
                {
                    MessageBox.Show("Image size must be " + imageDim.ToString() + "x" + imageDim.ToString() + " pixels", "Wrong image size", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {                    
                    int[,] patternPixels;
                    int p = 0;
                    int midColor = Math.Abs((int)(Color.Black.ToArgb() / 2));
                    Bitmap b = new Bitmap(imgPattern);
                    patternPixels = new int[imageDim, imageDim];
                    List<Neuron> pattern = new List<Neuron>(imageDim * imageDim);
                    for (int i = 0; i < imageDim; i++)
                        for (int j = 0; j < imageDim; j++)
                        {
							Neuron n = new Neuron();
                            p = Math.Abs(b.GetPixel(i, j).ToArgb());
                            if (p < midColor)
                            {
                                b.SetPixel(i, j, Color.White);
                                n.State = NeuronStates.AlongField;
                            }
                            else
                            {
                                b.SetPixel(i, j, Color.Black);
                                n.State = NeuronStates.AgainstField;
                            }
                            pattern.Add(n);
                        }
                    NN.AddPattern(pattern);
                    ImageMagnifier.ImageMagnifier im = new ImageMagnifier.ImageMagnifier();
                    im.ImageToMagnify = b;
                    im.MagnificationCoefficient = 8;
                    im.Location = new System.Drawing.Point(1, ((NN.M - 1) * (imageDim + 2) * im.MagnificationCoefficient));
                    im.Size = new System.Drawing.Size(imageDim * im.MagnificationCoefficient, imageDim * im.MagnificationCoefficient);
                    im.TabIndex = 0;
                    panelWzorceHopf.Controls.Add(im);
					butDodajTestHopf.Enabled = true;
                    butRozpoznajHopf.Enabled = false;
                }                
            }            
        }

        private void butRozpoznajHopf_Click(object sender, EventArgs e)
        {
			butRozpoznajHopf.Enabled = false;
			if (!patternSelected)
            {
                MessageBox.Show("Musisz wybrać wzór.", "Neural Network Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                List<Neuron> initialState = new List<Neuron>(NN.N);
                for (int i = 0; i < imageDim; i++)
                    for (int j=0;j<imageDim;j++)
                    {
						Neuron neuron = new Neuron();
                        if (imStanSieci.pixels[i, j] == Color.Black.ToArgb()) neuron.State = NeuronStates.AgainstField;
                        else if (imStanSieci.pixels[i, j] == Color.White.ToArgb()) neuron.State = NeuronStates.AlongField;
                        initialState.Add(neuron);
                    }
                NN.Run(initialState);
                patternSelected = false;
            }
        }

        private void HopfieldRecognizerMainForm_Load(object sender, EventArgs e)
        {
            butStworzSiecHopf.Text = "Stwórz sieć neuronową (" + (imageDim * imageDim) + " neuronów)";
        }

		private void butDodajTestHopf_Click(object sender, EventArgs e)
		{
			Image imgTest;
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				imgTest = Image.FromFile(ofd.FileName);
				if (imgTest.Width != imageDim || imgTest.Height != imageDim)
				{
					MessageBox.Show("Obraz musi mieć wymiary " + imageDim.ToString() + "x" + imageDim.ToString() + " pixels", "Wrong image size", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					Bitmap b = new Bitmap(imgTest);

					ImageMagnifier.ImageMagnifier im = new ImageMagnifier.ImageMagnifier();
					im.ImageToMagnify = b;
					imStanSieci.ImageToMagnify = im.ImageToMagnify;
					patternSelected = true;
					imStanSieci.Invalidate();
					butRozpoznajHopf.Enabled = true;
				}
			}
		}

		private void butStworzSiecHamm_Click(object sender, EventArgs e)
		{
			HNN = new HammingNeuralNetwork(imageDim * imageDim);
			panelWzorcowHamm.Controls.Clear();
	
			Random rnd = new Random();
			int r = 0;
			imHamming.pixels = new int[imageDim, imageDim];
			for (int i = 0; i < imageDim; i++)
				for (int j = 0; j < imageDim; j++)
				{
					r = rnd.Next(2);
					if (r == 0) imHamming.pixels[i, j] = Color.Black.ToArgb();
					else if (r == 1) imHamming.pixels[i, j] = Color.White.ToArgb();
				}
			patternSelectedHamm = false;
			butDodajWzorHamm.Enabled = true;
			butDodajTestHamm.Enabled = false;
			butRozpoznajHamm.Enabled = false;
			imHamming.Visible = true;
			imHamming.Invalidate();
		}

		private void butDodajWzorHamm_Click(object sender, EventArgs e)
		{
			Image imgPattern;
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				imgPattern = Image.FromFile(ofd.FileName);
				if (imgPattern.Width != imageDim || imgPattern.Height != imageDim)
				{
					MessageBox.Show("Obraz musi mieć wymiary " + imageDim.ToString() + "x" + imageDim.ToString() + " pixels", "Wrong image size", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					int[,] patternPixels;
					int p = 0;
					int midColor = Math.Abs((int)(Color.Black.ToArgb() / 2));
					Bitmap b = new Bitmap(imgPattern);
					patternPixels = new int[imageDim, imageDim];
					List<HammingNeuron> pattern = new List<HammingNeuron>(imageDim * imageDim);
					for (int i = 0; i < imageDim; i++)
						for (int j = 0; j < imageDim; j++)
						{
							HammingNeuron n = new HammingNeuron();
							p = Math.Abs(b.GetPixel(i, j).ToArgb());
							if (p < midColor)
							{
								b.SetPixel(i, j, Color.White);
								n.State = NeuronStates.AlongField;
							}
							else
							{
								b.SetPixel(i, j, Color.Black);
								n.State = NeuronStates.AgainstField;
							}
							pattern.Add(n);
						}
					HNN.AddPattern(pattern);
					
					ImageMagnifier.ImageMagnifier im = new ImageMagnifier.ImageMagnifier();
					im.ImageToMagnify = b;
					im.MagnificationCoefficient = 8;
					im.Location = new Point(1, ((HNN.M - 1) * (imageDim + 2) * im.MagnificationCoefficient));
					im.Size = new Size(imageDim * im.MagnificationCoefficient, imageDim * im.MagnificationCoefficient);
					im.TabIndex = 0;
					panelWzorcowHamm.Controls.Add(im);
					butDodajTestHamm.Enabled = true;
					butRozpoznajHamm.Enabled = false;
				}
			}
		}

		private void butDodajTestHamm_Click(object sender, EventArgs e)
		{
			Image imgTest;
			if (ofd.ShowDialog() == DialogResult.OK)
			{
				imgTest = Image.FromFile(ofd.FileName);
				if (imgTest.Width != imageDim || imgTest.Height != imageDim)
				{
					MessageBox.Show("Obraz musi mieć wymiary " + imageDim.ToString() + "x" + imageDim.ToString() + " pixels", "Wrong image size", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					Bitmap b = new Bitmap(imgTest);

					ImageMagnifier.ImageMagnifier im = new ImageMagnifier.ImageMagnifier();
					im.ImageToMagnify = b;
					imHamming.ImageToMagnify = im.ImageToMagnify;
					patternSelectedHamm = true;
					imHamming.Invalidate();
					butRozpoznajHamm.Enabled = true;
				}
			}
		}

		private void butRozpoznajHamm_Click(object sender, EventArgs e)
		{
			butRozpoznajHamm.Enabled = false;
			if (!patternSelectedHamm)
			{
				MessageBox.Show("Musisz wybrać wzór.", "Neural Network Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else
			{
				List<HammingNeuron> initialState = new List<HammingNeuron>(HNN.N);
				for (int i = 0; i < imageDim; i++)
					for (int j = 0; j < imageDim; j++)
					{
						HammingNeuron neuron = new HammingNeuron();
						if (imHamming.pixels[i, j] == Color.Black.ToArgb()) neuron.State = HammNeuronStates.AgainstField;
						else if (imHamming.pixels[i, j] == Color.White.ToArgb()) neuron.State = HammNeuronStates.AlongField;
						initialState.Add(neuron);
					}

				int[] wzorzec = HNN.Run(initialState);
	
				int[,] tablicaodwzorzec = new int[imageDim, imageDim];

				for (int i = 0; i < imageDim; i++)
				{
					for (int j = 0; j < imageDim; j++)
					{
						tablicaodwzorzec[i, j] = wzorzec[10 * i + j];
					}
				}

				imHamming.pixels = new int[imageDim, imageDim];
				for (int i = 0; i < imageDim; i++)
				{
					for (int j = 0; j < imageDim; j++)
					{
		
							if (tablicaodwzorzec[i,j] == -1)
							{
								imHamming.pixels[i, j] = Color.Black.ToArgb();
							}
							else if (tablicaodwzorzec[i, j] == 1)
							{
								imHamming.pixels[i, j] = Color.White.ToArgb();
							}
					}
				}
					
				butRozpoznajHamm.Enabled = false;
				imHamming.Visible = true;
				imHamming.Invalidate();

				patternSelectedHamm = false;
			}
		}
	}
}