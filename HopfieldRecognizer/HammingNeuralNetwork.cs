﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HopfieldRecognizer
{
	class HammingNeuralNetwork
	{
		private readonly int n;
		private int m;
		private double e;
		private readonly int[,] T;
		private List<int[]> wagipierwsze = new List<int[]>();
		private int[] wektorProgowy;
		private int[] wyjscieWarstw1;
		private double[] wejscieWarstw2;
		private double epsilon;
		private double[,] wagi2;
		private double[] wyjscieWarstw2;

		public int N
		{
			get { return N1; }
		}
		public int M
		{
			get { return M1; }
		}
		public double Energy
		{
			get { return E; }
		}
		public int[,] Matrix
		{
			get { return T; }
		}

		private void CalculateEnergy()
		{
			double tempE = 0;
			for (int i = 0; i < N1; i++)
				for (int j = 0; j < N1; j++)
					if (i != j)
						tempE += T[i, j] * Neurons[i].State * Neurons[j].State;
			E = -1 * tempE / 2;
		}

		public List<HammingNeuron> Neurons { get; private set; }
		
		public int N1 => N2;

		public int N2 => n;

		public double E { get => E1; set => E1 = value; }
		public int M1 { get => M2; set => M2 = value; }
		public double E1 { get => E2; set => E2 = value; }
		public int M2 { get => m; set => m = value; }
		public double E2 { get => e; set => e = value; }

		public HammingNeuralNetwork(int n)
		{
			this.n = n;
			Neurons = new List<HammingNeuron>(n);
			for (int i = 0; i < n; i++)
			{
				HammingNeuron neuron = new HammingNeuron();
				neuron.State = 0;
				Neurons.Add(neuron);
			}
			M1 = 0;
			

			//Warstwa 1
			//wagi przygotowywane na podstawie wzorców uczących - implementacja w kolejnym etapie
			wektorProgowy = new int[M1]; //musi być transponowane
			wyjscieWarstw1 = new int[M1]; //transponowane

			//Warstwa 2

			//wejscieWarstw2 = (double[])wyjscieWarstw1;
			epsilon = 1 / (M1 +1); // ε = (0; 1/M1-1)
			wagi2 = new double[M1, n];
			for (int i = 0; i < M1; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if(i==j)
					{
						wagi2[i, j] = 1;
					}
					else
					wagi2[i, j] = 0 - epsilon;
				}
			}

			wyjscieWarstw2 = new double[M1];

		}

		public void AddPattern(List<HammingNeuron> Pattern)
		{
			M1++;
			int[] wagi = new int[n];
			for (int i = 0; i < n; i++)
			{
				wagi[i] += (Pattern[i].State);
			}
			wagipierwsze.Add(wagi);
		}
		

		public void FreeMatrix()
		{
			for (int i = 0; i < N1; i++)
				for (int j = 0; j < N1; j++)
					T[i, j] = 0;
		}

		public int[] Run(List<HammingNeuron> initialState)
		{
			//Warstwa 1
			int[] mnozenie = new int[M1];
			for (int i = 0; i < M1; i++)
			{
				int[] aktualnawaga = wagipierwsze[i];
				int suma = 0;
				for (int j = 0; j < n; j++)
				{
					suma+=aktualnawaga[j] * initialState[j].State;
					
				}
				mnozenie[i] = suma;
			}
			wektorProgowy = new int[M1];
			wyjscieWarstw1 = new int[M1];
			for (int i = 0; i < M1; i++)
			{
				wektorProgowy[i] = n;
				wyjscieWarstw1[i] = mnozenie[i] + wektorProgowy[i];
			}

			//Warstwa 2
			
			wejscieWarstw2 = new double[wyjscieWarstw1.Length];
			for (int i = 0; i < wejscieWarstw2.Length; i++)
			{
				wejscieWarstw2[i] = (double)wyjscieWarstw1[i];
			}
				
			epsilon = (1.0 / ((double)M1 + 1.0)); // ε = (0; 1/M1-1)
			wagi2 = new double[M1, M1];
			for (int i = 0; i < M1; i++)
			{
				for (int j = 0; j < M1; j++)
				{
					if (i == j)
					{
						wagi2[i, j] = 1;
					}
					else
						wagi2[i, j] = 0 - epsilon;
				}
			}
			wyjscieWarstw2 = new double[M1];
			int licznik = 0;
			while (licznik!=1)
			{
				for (int i = 0; i < M1; i++)
				{
					double suma = 0;
					for (int j = 0; j < M1; j++)
					{
						if (i ==0)
						{
							suma += wagi2[i, j] * wejscieWarstw2[j];
						}
						else
						suma += wagi2[i, j] * wejscieWarstw2[j];
					}
					wejscieWarstw2[i] = suma;
					
				}

				for (int i = 0; i < wejscieWarstw2.Length; i++)
				{
					if (wejscieWarstw2[i] <= 0)
					{
						wejscieWarstw2[i] = 0;
					}
					licznik = 0;
					foreach (var item in wejscieWarstw2)
					{
						if (item != 0)
						{
							licznik++;
						}
					}
				}
				
			}
			int index = 0;
			for (int i = 0; i < wejscieWarstw2.Length; i++)
			{
				if (wejscieWarstw2[i]!=0)
				{
					index = i;
					break;
				}
			}

			int[] rozpoznanyWzorzec = wagipierwsze[index];


			return rozpoznanyWzorzec;
		}
		public double[] KopiujTab(double[] tab)
		{
			double[] tablica = new double[tab.Length];
			for (int i = 0; i < tablica.Length; i++)
			{
				tablica[i] = tab[i];
			}
			return tablica;
		}

		public event HammEnergyChangedHandler EnergyChanged;


		protected virtual void OnEnergyChanged(HammEnergyEventArgs e)
		{
			if (EnergyChanged != null)
				EnergyChanged(this, e);
		}
	}
	public delegate void HammEnergyChangedHandler(object sender, HammEnergyEventArgs e);


	public class HammEnergyEventArgs : EventArgs
	{
		private double energy;
		private int neuronIndex;

		public double Energy
		{
			get { return energy; }
		}
		public HammEnergyEventArgs(double Energy, int NeuronIndex)
		{
			this.energy = Energy;
			this.neuronIndex = NeuronIndex;

		}
		public int NeuronIndex
		{
			get { return neuronIndex; }
		}
	}

}


