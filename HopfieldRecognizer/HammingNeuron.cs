﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HopfieldRecognizer
{
	class HammingNeuron
	{
		private int state;

		public int State
		{
			get { return state; }
			set { state = value; }
		}

		public HammingNeuron()
		{
			int r = new Random().Next(2);
			switch (r)
			{
				case 0: state = HammNeuronStates.AlongField; break;
				case 1: state = HammNeuronStates.AgainstField; break;
			}
		}

		public bool ChangeState(Double field)
		{
			bool res = false;
			if (field * this.State < 0)
			{
				this.state = -this.state;
				res = true;
			}
			return res;
		}
	}

	public static class HammNeuronStates
	{
		/// <summary>
		/// If neuron orienatated along local field, then it's state is equal to 1
		/// </summary>
		public static int AlongField = 1;
		/// <summary>
		/// If neuron orienatated against local field, then it's state is equal to -1
		/// </summary>
		public static int AgainstField = -1;
	}
}
