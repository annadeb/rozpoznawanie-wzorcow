﻿namespace HopfieldRecognizer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
			this.butStworzSiecHopf = new System.Windows.Forms.Button();
			this.butDodajWzor = new System.Windows.Forms.Button();
			this.panelWzorceHopf = new System.Windows.Forms.Panel();
			this.butRozpoznajHopf = new System.Windows.Forms.Button();
			this.gbStanSieci = new System.Windows.Forms.GroupBox();
			this.imStanSieci = new ImageMagnifier.ImageMagnifier();
			this.imHamming = new ImageMagnifier.ImageMagnifier();
			this.ofd = new System.Windows.Forms.OpenFileDialog();
			this.gbWgraneWzorce = new System.Windows.Forms.GroupBox();
			this.labSiecHopf = new System.Windows.Forms.Label();
			this.butDodajTestHopf = new System.Windows.Forms.Button();
			this.labSiecHamm = new System.Windows.Forms.Label();
			this.gbWgraneWzorceHamm = new System.Windows.Forms.GroupBox();
			this.panelWzorcowHamm = new System.Windows.Forms.Panel();
			this.butStworzSiecHamm = new System.Windows.Forms.Button();
			this.butDodajWzorHamm = new System.Windows.Forms.Button();
			this.butDodajTestHamm = new System.Windows.Forms.Button();
			this.gbStanSieciHamm = new System.Windows.Forms.GroupBox();
			this.butRozpoznajHamm = new System.Windows.Forms.Button();
			this.gbStanSieci.SuspendLayout();
			this.gbWgraneWzorce.SuspendLayout();
			this.gbWgraneWzorceHamm.SuspendLayout();
			this.gbStanSieciHamm.SuspendLayout();
			this.SuspendLayout();
			// 
			// butStworzSiecHopf
			// 
			this.butStworzSiecHopf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butStworzSiecHopf.Location = new System.Drawing.Point(8, 54);
			this.butStworzSiecHopf.Name = "butStworzSiecHopf";
			this.butStworzSiecHopf.Size = new System.Drawing.Size(216, 23);
			this.butStworzSiecHopf.TabIndex = 1;
			this.butStworzSiecHopf.Text = "Stwórz sieć neuronową";
			this.butStworzSiecHopf.UseVisualStyleBackColor = true;
			this.butStworzSiecHopf.Click += new System.EventHandler(this.butStworzSiecHopf_Click);
			// 
			// butDodajWzor
			// 
			this.butDodajWzor.Enabled = false;
			this.butDodajWzor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butDodajWzor.Location = new System.Drawing.Point(8, 83);
			this.butDodajWzor.Name = "butDodajWzor";
			this.butDodajWzor.Size = new System.Drawing.Size(216, 23);
			this.butDodajWzor.TabIndex = 1;
			this.butDodajWzor.Text = "Dodaj wzór do sieci neuronowej";
			this.butDodajWzor.UseVisualStyleBackColor = true;
			this.butDodajWzor.Click += new System.EventHandler(this.butDodajWzor_Click);
			// 
			// panelWzorceHopf
			// 
			this.panelWzorceHopf.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.panelWzorceHopf.AutoScroll = true;
			this.panelWzorceHopf.Location = new System.Drawing.Point(3, 19);
			this.panelWzorceHopf.Name = "panelWzorceHopf";
			this.panelWzorceHopf.Size = new System.Drawing.Size(105, 340);
			this.panelWzorceHopf.TabIndex = 0;
			// 
			// butRozpoznajHopf
			// 
			this.butRozpoznajHopf.Enabled = false;
			this.butRozpoznajHopf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butRozpoznajHopf.Location = new System.Drawing.Point(15, 396);
			this.butRozpoznajHopf.Name = "butRozpoznajHopf";
			this.butRozpoznajHopf.Size = new System.Drawing.Size(216, 23);
			this.butRozpoznajHopf.TabIndex = 4;
			this.butRozpoznajHopf.Text = "Rozpoznaj";
			this.butRozpoznajHopf.UseVisualStyleBackColor = true;
			this.butRozpoznajHopf.Click += new System.EventHandler(this.butRozpoznajHopf_Click);
			// 
			// gbStanSieci
			// 
			this.gbStanSieci.Controls.Add(this.imStanSieci);
			this.gbStanSieci.Location = new System.Drawing.Point(8, 160);
			this.gbStanSieci.Name = "gbStanSieci";
			this.gbStanSieci.Size = new System.Drawing.Size(236, 230);
			this.gbStanSieci.TabIndex = 5;
			this.gbStanSieci.TabStop = false;
			this.gbStanSieci.Text = "Sieć neuronowa";
			// 
			// imStanSieci
			// 
			this.imStanSieci.ImageToMagnify = ((System.Drawing.Image)(resources.GetObject("imStanSieci.ImageToMagnify")));
			this.imStanSieci.Location = new System.Drawing.Point(7, 19);
			this.imStanSieci.MagnificationCoefficient = 20;
			this.imStanSieci.Name = "imStanSieci";
			this.imStanSieci.Size = new System.Drawing.Size(200, 200);
			this.imStanSieci.TabIndex = 0;
			this.imStanSieci.Text = "imStanSieci";
			this.imStanSieci.Visible = false;
			// 
			// imHamming
			// 
			this.imHamming.ImageToMagnify = ((System.Drawing.Image)(resources.GetObject("imHamming.ImageToMagnify")));
			this.imHamming.Location = new System.Drawing.Point(20, 19);
			this.imHamming.MagnificationCoefficient = 20;
			this.imHamming.Name = "imHamming";
			this.imHamming.Size = new System.Drawing.Size(200, 200);
			this.imHamming.TabIndex = 0;
			this.imHamming.Text = "imageMagnifier1";
			this.imHamming.Visible = false;
			// 
			// gbWgraneWzorce
			// 
			this.gbWgraneWzorce.Controls.Add(this.panelWzorceHopf);
			this.gbWgraneWzorce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.gbWgraneWzorce.Location = new System.Drawing.Point(247, 54);
			this.gbWgraneWzorce.Name = "gbWgraneWzorce";
			this.gbWgraneWzorce.Size = new System.Drawing.Size(108, 365);
			this.gbWgraneWzorce.TabIndex = 0;
			this.gbWgraneWzorce.TabStop = false;
			this.gbWgraneWzorce.Text = "Wgrane wzorce";
			// 
			// labSiecHopf
			// 
			this.labSiecHopf.AutoSize = true;
			this.labSiecHopf.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labSiecHopf.Location = new System.Drawing.Point(121, 9);
			this.labSiecHopf.Name = "labSiecHopf";
			this.labSiecHopf.Size = new System.Drawing.Size(151, 25);
			this.labSiecHopf.TabIndex = 6;
			this.labSiecHopf.Text = "Sieć Hopfielda";
			// 
			// butDodajTestHopf
			// 
			this.butDodajTestHopf.Enabled = false;
			this.butDodajTestHopf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butDodajTestHopf.Location = new System.Drawing.Point(8, 112);
			this.butDodajTestHopf.Name = "butDodajTestHopf";
			this.butDodajTestHopf.Size = new System.Drawing.Size(216, 23);
			this.butDodajTestHopf.TabIndex = 7;
			this.butDodajTestHopf.Text = "Dodaj obraz testowy";
			this.butDodajTestHopf.UseVisualStyleBackColor = true;
			this.butDodajTestHopf.Click += new System.EventHandler(this.butDodajTestHopf_Click);
			// 
			// labSiecHamm
			// 
			this.labSiecHamm.AutoSize = true;
			this.labSiecHamm.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labSiecHamm.Location = new System.Drawing.Point(449, 9);
			this.labSiecHamm.Name = "labSiecHamm";
			this.labSiecHamm.Size = new System.Drawing.Size(162, 25);
			this.labSiecHamm.TabIndex = 8;
			this.labSiecHamm.Text = "Sieć Hamminga";
			// 
			// gbWgraneWzorceHamm
			// 
			this.gbWgraneWzorceHamm.Controls.Add(this.panelWzorcowHamm);
			this.gbWgraneWzorceHamm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.gbWgraneWzorceHamm.Location = new System.Drawing.Point(638, 54);
			this.gbWgraneWzorceHamm.Name = "gbWgraneWzorceHamm";
			this.gbWgraneWzorceHamm.Size = new System.Drawing.Size(108, 365);
			this.gbWgraneWzorceHamm.TabIndex = 9;
			this.gbWgraneWzorceHamm.TabStop = false;
			this.gbWgraneWzorceHamm.Text = "Wgrane wzorce";
			// 
			// panelWzorcowHamm
			// 
			this.panelWzorcowHamm.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.panelWzorcowHamm.AutoScroll = true;
			this.panelWzorcowHamm.Location = new System.Drawing.Point(3, 19);
			this.panelWzorcowHamm.Name = "panelWzorcowHamm";
			this.panelWzorcowHamm.Size = new System.Drawing.Size(105, 340);
			this.panelWzorcowHamm.TabIndex = 0;
			// 
			// butStworzSiecHamm
			// 
			this.butStworzSiecHamm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butStworzSiecHamm.Location = new System.Drawing.Point(404, 54);
			this.butStworzSiecHamm.Name = "butStworzSiecHamm";
			this.butStworzSiecHamm.Size = new System.Drawing.Size(216, 23);
			this.butStworzSiecHamm.TabIndex = 10;
			this.butStworzSiecHamm.Text = "Stwórz sieć neuronową";
			this.butStworzSiecHamm.UseVisualStyleBackColor = true;
			this.butStworzSiecHamm.Click += new System.EventHandler(this.butStworzSiecHamm_Click);
			// 
			// butDodajWzorHamm
			// 
			this.butDodajWzorHamm.Enabled = false;
			this.butDodajWzorHamm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butDodajWzorHamm.Location = new System.Drawing.Point(404, 83);
			this.butDodajWzorHamm.Name = "butDodajWzorHamm";
			this.butDodajWzorHamm.Size = new System.Drawing.Size(216, 23);
			this.butDodajWzorHamm.TabIndex = 11;
			this.butDodajWzorHamm.Text = "Dodaj wzór do sieci neuronowej";
			this.butDodajWzorHamm.UseVisualStyleBackColor = true;
			this.butDodajWzorHamm.Click += new System.EventHandler(this.butDodajWzorHamm_Click);
			// 
			// butDodajTestHamm
			// 
			this.butDodajTestHamm.Enabled = false;
			this.butDodajTestHamm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butDodajTestHamm.Location = new System.Drawing.Point(404, 112);
			this.butDodajTestHamm.Name = "butDodajTestHamm";
			this.butDodajTestHamm.Size = new System.Drawing.Size(216, 23);
			this.butDodajTestHamm.TabIndex = 12;
			this.butDodajTestHamm.Text = "Dodaj obraz testowy";
			this.butDodajTestHamm.UseVisualStyleBackColor = true;
			this.butDodajTestHamm.Click += new System.EventHandler(this.butDodajTestHamm_Click);
			// 
			// gbStanSieciHamm
			// 
			this.gbStanSieciHamm.Controls.Add(this.imHamming);
			this.gbStanSieciHamm.Location = new System.Drawing.Point(399, 160);
			this.gbStanSieciHamm.Name = "gbStanSieciHamm";
			this.gbStanSieciHamm.Size = new System.Drawing.Size(236, 230);
			this.gbStanSieciHamm.TabIndex = 13;
			this.gbStanSieciHamm.TabStop = false;
			this.gbStanSieciHamm.Text = "Sieć neuronowa";
			// 
			// butRozpoznajHamm
			// 
			this.butRozpoznajHamm.Enabled = false;
			this.butRozpoznajHamm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.butRozpoznajHamm.Location = new System.Drawing.Point(404, 396);
			this.butRozpoznajHamm.Name = "butRozpoznajHamm";
			this.butRozpoznajHamm.Size = new System.Drawing.Size(216, 23);
			this.butRozpoznajHamm.TabIndex = 14;
			this.butRozpoznajHamm.Text = "Rozpoznaj";
			this.butRozpoznajHamm.UseVisualStyleBackColor = true;
			this.butRozpoznajHamm.Click += new System.EventHandler(this.butRozpoznajHamm_Click);
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(758, 429);
			this.Controls.Add(this.gbStanSieciHamm);
			this.Controls.Add(this.butRozpoznajHamm);
			this.Controls.Add(this.butDodajTestHamm);
			this.Controls.Add(this.butDodajWzorHamm);
			this.Controls.Add(this.butStworzSiecHamm);
			this.Controls.Add(this.gbWgraneWzorceHamm);
			this.Controls.Add(this.labSiecHamm);
			this.Controls.Add(this.butStworzSiecHopf);
			this.Controls.Add(this.butDodajTestHopf);
			this.Controls.Add(this.labSiecHopf);
			this.Controls.Add(this.gbStanSieci);
			this.Controls.Add(this.butRozpoznajHopf);
			this.Controls.Add(this.butDodajWzor);
			this.Controls.Add(this.gbWgraneWzorce);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Rozpoznawanie wzorców 1.0";
			this.Load += new System.EventHandler(this.HopfieldRecognizerMainForm_Load);
			this.gbStanSieci.ResumeLayout(false);
			this.gbWgraneWzorce.ResumeLayout(false);
			this.gbWgraneWzorceHamm.ResumeLayout(false);
			this.gbStanSieciHamm.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butStworzSiecHopf;
        private System.Windows.Forms.Button butDodajWzor;
        private System.Windows.Forms.Panel panelWzorceHopf;
        private System.Windows.Forms.Button butRozpoznajHopf;
        private System.Windows.Forms.GroupBox gbStanSieci;
        private System.Windows.Forms.OpenFileDialog ofd;
        private ImageMagnifier.ImageMagnifier imStanSieci;
        private System.Windows.Forms.GroupBox gbWgraneWzorce;
		private System.Windows.Forms.Label labSiecHopf;
		private System.Windows.Forms.Button butDodajTestHopf;
		private System.Windows.Forms.Label labSiecHamm;
		private System.Windows.Forms.GroupBox gbWgraneWzorceHamm;
		private System.Windows.Forms.Panel panelWzorcowHamm;
		private System.Windows.Forms.Button butStworzSiecHamm;
		private System.Windows.Forms.Button butDodajWzorHamm;
		private System.Windows.Forms.Button butDodajTestHamm;
		private System.Windows.Forms.GroupBox gbStanSieciHamm;
		private ImageMagnifier.ImageMagnifier imHamming;
		private System.Windows.Forms.Button butRozpoznajHamm;
	}
}